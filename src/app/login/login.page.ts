import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  login = new FormGroup({
    correo: new FormControl('', ),
    contra: new FormControl('', Validators.minLength(8))
  });
  constructor(private router: Router) { }

  ngOnInit() {
  }
  // navegar entre pantallas usando una función
  async navegarInicio(){
    this.router.navigate(['/inicio']);
  }

  navegarRegistro() {
    this.router.navigate(['/registrar']);
  }

}
