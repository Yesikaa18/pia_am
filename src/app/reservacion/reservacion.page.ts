import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, MenuController, NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Doctores } from './../doctores.model';
import { DoctorsService } from './../doctors.service';


@Component({
  selector: 'app-reservacion',
  templateUrl: './reservacion.page.html',
  styleUrls: ['./reservacion.page.scss'],
})
export class ReservacionPage implements OnInit {
 // doctores: Doctores[];

/*  paciente = {
    nombre: '',
    apellido: '',
    edad: '',
    telefono: ''
  }; */

  paciente = new FormGroup({
    nombre: new FormControl('', ),
    apellido: new FormControl('', ),
    edad: new FormControl('', Validators.min(0)),
    telefono: new FormControl('', Validators.maxLength(10))
  });


  constructor(private doctorsService: DoctorsService, private alertCtrl: AlertController, private router: Router,
              private route: ActivatedRoute, private menu: MenuController, private navCtrl: NavController) {}

  ngOnInit() {

  }

  openMenu(){
    this.menu.open();
  }

  async onSubmitTemplate(){
    console.log(this.paciente);

    const alert = await this.alertCtrl.create({
      // cssClass: 'my-custom-class',
      header: '¡Tu cita se ha confirmado exitosamente!',
      subHeader: 'No. de consulta: 12',
      message: 'Presentar alguna identificación a la hora de ser atendido',
      buttons: ['OK']
    });

    await alert.present();

    this.router.navigate(['/inicio']);
  }



}
