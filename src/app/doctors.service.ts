import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Doctores } from './doctores.model';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {
  constructor(private http: HttpClient){}

  private api = './../assets/doctores';


  /* getDoctores(){
    this.http.get('./../assets/doctores/doctores.json')
    .subscribe(resp => {
      this.api = resp;
      this.conf = true;
    });
  } */

  getDoctores(){
    const path = `${this.api}/doctores.json`;
    return this.http.get<Doctores[]>(path);
  }

  getDoctor(id: string){
    const path = `${this.api}/doctores.json/${id}`;
    return this.http.get<Doctores>(path);
  }



}
