export interface Doctores{
    id: string;
    nombre: string;
    especialidad: string;
    estado: string;
    photo: string;
    punto: string;
    direccion: string;
    horarios: string;
}
