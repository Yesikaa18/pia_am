import { DoctorsService } from './../doctors.service';
import { Component, OnInit } from '@angular/core';
import { Doctores } from './../doctores.model';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, MenuController, NavController, NavParams } from '@ionic/angular';
import { ReservacionPage } from '../reservacion/reservacion.page';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit{

  reservacionPage = ReservacionPage;
  especialidad: any;
  doctores: Doctores[] = [];

  constructor(private doctorsService: DoctorsService,
              private menu: MenuController,
              private router: Router,
              private navCtrl: NavController,
              private activatedRoute: ActivatedRoute,
              private loadingCtrl: LoadingController) { }

  ngOnInit() {

    this.presentLoading();
    this.doctorsService.getDoctores()
    .subscribe(doctores => {
      console.log(doctores);
      this.doctores = doctores;
    });

  }
  async presentLoading(){
    const loading = await this.loadingCtrl.create({
      message: 'Cargando...',
      duration: 2000
    });
    return await loading.present();
  }

  openMenu(){
    this.menu.open();
  }

/*  reservarCita(){
    this.router.navigate(['/reservacion']);

  } */
  }

