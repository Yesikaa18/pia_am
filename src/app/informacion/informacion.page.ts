import { Component, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.page.html',
  styleUrls: ['./informacion.page.scss'],
})
export class InformacionPage implements OnInit {

  parametros = {
    id: null
  };

  doctores = {
    id: '',
    nombre: '',
    especialidad: '',
    estado: '',
    photo: '',
    direccion: '',
    horarios: ''
  };
  constructor(private navCtrl: NavController,
              private menu: MenuController,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private loadingCtrl: LoadingController){}

  ngOnInit() {
    this.presentLoading();
    this.parametros.id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.parametros.id);
    // tslint:disable-next-line:prefer-const
    var api = './../../assets/doctores/doctores.json';

    fetch(api).then(res => res.json())
    .then(json => {
      this.doctores = json[this.parametros.id - 1];
    });
  }

  async presentLoading(){
    const loading = await this.loadingCtrl.create({
      message: 'Cargando...',
      duration: 2000
    });
    return await loading.present();
  }
  openMenu(){
    this.menu.open();
  }

 /* reservarCita(){
    this.router.navigate(['/reservacion']);
  } */

}
