import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.page.html',
  styleUrls: ['./registrar.page.scss'],
})
export class RegistrarPage implements OnInit {

  constructor(private alertCtrl: AlertController,
              private router: Router,
              private menu: MenuController,
              private loadingCtrl: LoadingController) { }

 /* usuario = {
    correo: '',
    contra: '',
    confContra: '',
    nombre: '',
    apellidos: ''
  }; */

  usuario = new FormGroup({
    correo: new FormControl('', ),
    contra: new FormControl('', Validators.minLength(8)),
    confContra: new FormControl('', Validators.minLength(8)),
    nombre: new FormControl('', ),
    apellidos: new FormControl('', )
  });
  ngOnInit() {
    this.presentLoading();
  }

  async presentLoading(){
    const loading = await this.loadingCtrl.create({
      message: 'Cargando...',
      duration: 2000
    });
    return await loading.present();
  }
  openMenu(){
    this.menu.open();
  }


  async onSubmitTemplate(){
    console.log(this.usuario);
    const alert = await this.alertCtrl.create({
      // cssClass: 'my-custom-class',
      header: '¡Tu registro se ha confirmado exitosamente!',
    //  subHeader: 'Subtitle',
      message: 'Inicie sesión para entrar a su cuenta',
      buttons: ['OK']
    });

    await alert.present();
    this.router.navigate(['/login']);
  }

}
